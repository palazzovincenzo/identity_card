var disable = true;
var buttoTab = document.getElementById("eliot");
var pannelloAbilitato = null;

function gestisciTablePane(tablepane){
  if(disable == true){
    tablepane.style.display = "none";
  } else {
    tablepane.style.display = "block";
  }
}

function azioneAbilitaEliot(){
  if(disable == true){
    disable = false;
  }else {
    disable = true;
  }
  pannelloAbilitato = "eliot";
  var tableDescrizione = document.getElementById("desc_tab_eliot");
  gestisciTablePane(tableDescrizione);
}

function initTable() {
  disable = true;
  var tableDescrizione = document.getElementById("desc_tab_eliot");
  gestisciTablePane(tableDescrizione);
}

function nascondiPane(){
  disable = false;
  if(pannelloAbilitato == "eliot"){
    azioneAbilitaEliot();
  }
}
