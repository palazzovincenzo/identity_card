function compilaEliot(){
  console.log("Compilo dati per Eliot Anderson");
  document.getElementById("name_tex_in").value = "Eliot";
  document.getElementById("cognome_tex_in").value = "Anderson";
  document.getElementById("nato_tex_gg").value = "xx";
  document.getElementById("nato_tex_mm").value = "xx";
  document.getElementById("nato_tex_aaaa").value = "xxxx";
  document.getElementById("atto_text_in_uno").value = "xxxx";
  document.getElementById("p_text_int_uno").value = "xxxx";
  document.getElementById("s_text_int_uno").value = "xxxx";
  document.getElementById("a_text_int").value = "xxxx";
  document.getElementById("parentesi_aperta_text_int_uno").value = "xxxx";
  document.getElementById("cittadinanza_tex_in").value = "Americana";
  document.getElementById("residenza_tex_in").value = "New York";
  document.getElementById("stato_civile_tex_in").value = "xxx";
  document.getElementById("professione_tex_in").value = "Sistemista";
  document.getElementById("statura_tex_in").value = "174 cm";
  document.getElementById("capelli_tex_in").value = "Neri";
  document.getElementById("occhi_tex_in").value = "Verdi";
  document.getElementById("segni_particolari_tex_in_uno").value = "Felpa nera con";
  document.getElementById("segni_particolari_tex_in_due").value = "cerniera e cappuccio";
  document.getElementById("firma_tex_uno").value = "Eliot Anderson";
  document.getElementById("firma_tex_tre").value = "25/12/2018";
  document.getElementById("firma_sindaco_text_int").value = "Santa Claus";
  //load immagini
  document.getElementById("foto_card").src = "img/eliot.jpg";
  document.getElementById("imp_digitale").src = "img/eliot_imp.png";
  document.getElementById("spazio_timbro_comunale").src = "img/fake.png";

}

function init(chiCompilare){
  if(chiCompilare == null){
    console.log("Dovevo compilare la carta d. vuota");
  }
  compilaEliot();
}
